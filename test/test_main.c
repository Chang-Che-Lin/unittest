#include "../Inc/gpio.h"
#include "unittest_transport.h"
#include <unity.h>

#define LD4_Pin GPIO_PIN_12
#define LD4_GPIO_Port GPIOD

void setUp(void) {
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  __HAL_RCC_GPIOD_CLK_ENABLE();

  GPIO_InitStruct.Pin = LD4_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(LD4_GPIO_Port, &GPIO_InitStruct);
}

void tearDown(void) {
  HAL_GPIO_DeInit(LD4_GPIO_Port, LD4_Pin);
}

void test_builtin_pin_number(void) {
  TEST_ASSERT_EQUAL(GPIO_PIN_12, LD4_Pin);
}

void test_state_high(void) {
  HAL_GPIO_WritePin(LD4_GPIO_Port, LD4_Pin, GPIO_PIN_SET);
  TEST_ASSERT_EQUAL(GPIO_PIN_SET, HAL_GPIO_ReadPin(LD4_GPIO_Port, LD4_Pin));
}

void test_state_low(void) {
  HAL_GPIO_WritePin(LD4_GPIO_Port, LD4_Pin, GPIO_PIN_RESET);
  TEST_ASSERT_EQUAL(GPIO_PIN_RESET, HAL_GPIO_ReadPin(LD4_GPIO_Port, LD4_Pin));
}

void test_state_toggle(void) {
  HAL_GPIO_WritePin(LD4_GPIO_Port, LD4_Pin, GPIO_PIN_RESET);
  HAL_GPIO_TogglePin(LD4_GPIO_Port, LD4_Pin);
  TEST_ASSERT_EQUAL(GPIO_PIN_SET, HAL_GPIO_ReadPin(LD4_GPIO_Port, LD4_Pin));
  HAL_Delay(500);
}

int main() {
  HAL_Init();
  HAL_Delay(2000); // service delay
  UNITY_BEGIN();
  RUN_TEST(test_builtin_pin_number);
  HAL_GPIO_WritePin(LD4_GPIO_Port, LD4_Pin, GPIO_PIN_SET);

  for (unsigned int i = 0; i < 5; i++) {
    RUN_TEST(test_state_high);
    HAL_Delay(500);
    RUN_TEST(test_state_low);
    HAL_Delay(500);
  }
  UNITY_END(); // stop unit testing

  while (1) {
  }
}

void SysTick_Handler(void) {
  HAL_IncTick();
}