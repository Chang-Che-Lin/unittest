#include "../Inc/transport.h"
#include "stm32f4xx_hal.h"

#define USARTx USART1

#define USARTx_TX_PIN GPIO_PIN_6
#define USARTx_TX_GPIO_PORT GPIOB
#define USARTx_TX_AF GPIO_AF7_USART1
#define USARTx_RX_PIN GPIO_PIN_7
#define USARTx_RX_GPIO_PORT GPIOB
#define USARTx_RX_AF GPIO_AF7_USART1
#define __HAL_RCC_USARTX_CLK_ENABLE __HAL_RCC_USART1_CLK_ENABLE
#define __HAL_RCC_GPIOX_CLK_ENABLE __HAL_RCC_GPIOB_CLK_ENABLE

UART_HandleTypeDef huart1;

void unittest_uart_begin() {
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  __HAL_RCC_USARTX_CLK_ENABLE();

  __HAL_RCC_GPIOX_CLK_ENABLE();

  GPIO_InitStruct.Pin = USARTx_TX_PIN;
  GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  GPIO_InitStruct.Speed = GPIO_SPEED_HIGH;
  GPIO_InitStruct.Alternate = USARTx_TX_AF;
  HAL_GPIO_Init(USARTx_TX_GPIO_PORT, &GPIO_InitStruct);

  GPIO_InitStruct.Pin = USARTx_RX_PIN;
  GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  GPIO_InitStruct.Speed = GPIO_SPEED_HIGH;
  GPIO_InitStruct.Alternate = USARTx_RX_AF;
  HAL_GPIO_Init(USARTx_RX_GPIO_PORT, &GPIO_InitStruct);

  huart1.Instance = USARTx;
  huart1.Init.BaudRate = 115200;
  huart1.Init.WordLength = UART_WORDLENGTH_8B;
  huart1.Init.StopBits = UART_STOPBITS_1;
  huart1.Init.Parity = UART_PARITY_NONE;
  huart1.Init.Mode = UART_MODE_TX_RX;
  huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart1.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart1) != HAL_OK) {
  }
}

void unittest_uart_putchar(char c) {
  HAL_UART_Transmit(&huart1, (uint8_t *)(&c), 1, 1000);
}

void unittest_uart_flush() {}

void unittest_uart_end() {
  __HAL_RCC_USARTX_CLK_ENABLE();

  HAL_GPIO_DeInit(USARTx_TX_GPIO_PORT, USARTx_TX_PIN | USARTx_RX_PIN);
}